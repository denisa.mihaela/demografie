// here goes the script
var ctx = document.getElementById("myChart").getContext("2d");
var chart = new Chart(ctx, {
  // The type of chart we want to create
  type: "line",

  // The data for our dataset
  data: {
    labels: [
      "1790",
      "1800",
      "1810",
      "1820",
      "1830",
      "1840",
      "1850",
      "1860",
      "1870",
      "1880",
      "1890",
      "1900",
      "1910",
      "1920",
      "1930",
      "1940",
      "1950",
      "1960",
      "1970",
      "1980",
      "1990",
      "2000",
      "2010",
      "2020"
    ],
    datasets: [
      {
        label: "S.U.A. - Populația de-a lungul timpului",
        backgroundColor: "rgba(255, 99, 132,0)",
        borderColor: "rgb(166,33,57)",
        borderWidth: 5,
        pointBorderWidth: 10,
        pointHoverBorderWidth: 12,
        data: [
          3929326,
          5308483,
          7239881,
          9638453,
          12866020,
          17069453,
          23191876,
          31443321,
          39818449,
          50189209,
          62947714,
          76212168,
          92228496,
          106021537,
          122775046,
          132164569,
          150697361,
          179323175,
          203302031,
          226545805,
          248709873,
          281421906,
          308745538,
          333546000
        ]
      }
    ]
  },

  // Configuration options go here
  options: {}
});
